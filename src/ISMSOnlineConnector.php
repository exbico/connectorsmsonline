<?php
/**
 * Created by PhpStorm.
 * User: Vlad Kor
 * Date: 18.05.2017
 * Time: 14:19
 */

namespace exbico\gateway\smsonline;


interface ISMSOnlineConnector
{
    public function init(string $user, string $secretKey, string $from);

    public function run(string $phone, string $message): bool;

    public function getErrors(): string;

    public function getResponseText(): string;

}