<?php
/**
 * Created by PhpStorm.
 * User: Vlad Kor
 * Date: 19.05.2017
 * Time: 12:08
 */

namespace exbico\gateway\smsonline;


class SMSOnlineRequestBuilder
{
    protected $url;
    protected $user;
    protected $secretKey;
    protected $from;
    protected $sign;
    protected $phone;
    protected $message;

    public function setSenderParams(string $phone, string $message, $params): string
    {
        $this->user = $params['user'];
        $this->secretKey = $params['secretKey'];
        $this->from = $params['from'];
        $this->phone = $this->prepPhone($phone);
        $this->message = $message;
        return $this->createPostParamString();
    }

    protected function prepPhone(string $phone)
    {
        if (!preg_match('/^\+(\d){11}$/', $phone)) {
            throw new SmsOnlineException('phone format is invalid');
        }
        return str_replace('+', '', $phone);
    }

    protected function createPostParamString()
    {
        $sign = md5($this->user . $this->from . $this->phone . $this->message . $this->secretKey);
        return "user={$this->user}&from={$this->from}&phone={$this->phone}&txt={$this->message}&sign={$sign}";
    }

}