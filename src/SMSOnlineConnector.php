<?php

/**
 * Created by PhpStorm.
 * User: Vlad Kor
 * Date: 05.05.2017
 * Time: 17:35
 */

namespace exbico\gateway\smsonline;


class SMSOnlineConnector implements ISMSOnlineConnector
{
    const SMS_ONLINE_SERVICE_URL = 'https://bulk.sms-online.com/';
    protected $config;
    /**@var SMSOnlineResponse $smsOnlineResponce */
    protected $smsOnlineResponce;
    protected $data;
    protected $errors;

    public function init(string $user, string $secretKey, string $from)
    {
        if (!isset($user, $secretKey, $from)) {
            throw new SmsOnlineException('Can\'t init SmsOnline. Undefined config.');
        }
        $this->config['user'] = $user;
        $this->config['secretKey'] = $secretKey;
        $this->config['from'] = $from;
    }

    public function run(string $phone, string $message): bool
    {
        try {
            $result = false;
            $this->data = $this->getRequestParams($phone, $message, $this->config);
            $this->smsOnlineResponce = $this->sendRequest();
            if (!$this->smsOnlineResponce->hasErrors()) {
                $result = true;
            }
            return $result;
        } catch (\Exception $e) {
            throw new SmsOnlineException('Can\'t send message: ' . $e->getMessage());
        }
    }

    public function getResponseText(): string
    {
        return $this->smsOnlineResponce->getResponseMessage();
    }

    public function getErrors(): string
    {
        return $this->smsOnlineResponce->getErrors();
    }

    protected function sendRequest(): SMSOnlineResponse
    {
        $smsOnlinePostRequest = new SMSOnlinePostRequest();
        return $smsOnlinePostRequest->execPostToSMSOnline(self::SMS_ONLINE_SERVICE_URL, $this->data);
    }

    protected function getRequestParams($phone, $message, $params)
    {
        $smsOnlineRequestBuilder = new SMSOnlineRequestBuilder();
        return $smsOnlineRequestBuilder->setSenderParams($phone, $message, $params);
    }


}