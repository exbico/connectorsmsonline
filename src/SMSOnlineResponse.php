<?php
/**
 * Created by PhpStorm.
 * User: Vlad Kor
 * Date: 16.05.2017
 * Time: 17:22
 */

namespace exbico\gateway\smsonline;


class SMSOnlineResponse
{
    const CODE_ERROR_OF_PARCER = '-90';
    const MESSAGE_ERROR_OF_PARCER = 'Answer can\'t parse';

    protected $responseCode;
    protected $responseMessage;

    protected $hasError = false;

    public function setResponse(string $answer)
    {
        $xml = new \SimpleXMLElement($answer);
        $xpathCode = $xml->xpath('./code');
        $xpathMessage = $xml->xpath('./tech_message');
        $code = self::CODE_ERROR_OF_PARCER;
        $message = self::MESSAGE_ERROR_OF_PARCER;
        if ($xpathCode !== false && $xpathMessage !== false) {
            $code = trim(array_shift($xpathCode));
            $message = trim(array_shift($xpathMessage));
        }
        $this->responseCode = $code;
        $this->responseMessage = $message;
    }

    public function getResponseMessage(): string
    {
        return $this->responseMessage;
    }

    public function checkErrors()
    {
        if ($this->responseCode !== '0') {
            $this->hasError = true;
        }
    }

    public function hasErrors(): bool
    {
        return $this->hasError;
    }

    public function getErrors()
    {
        return $this->hasError ? "{$this->responseCode} {$this->responseMessage}" : '';
    }


}