<?php
/**
 * Created by PhpStorm.
 * User: Vlad Kor
 * Date: 16.05.2017
 * Time: 14:30
 */

namespace exbico\gateway\smsonline;


class SMSOnlinePostRequest extends \Curl
{
    public $options = [
        CURLOPT_VERBOSE     => false,
        CURLINFO_HEADER_OUT => false,
        /*CURLOPT_CERTINFO    => false,*/
    ];

    public function execPostToSMSOnline($url, $data ): SMSOnlineResponse
    {
        $answer = new SMSOnlineResponse();
        $response = $this->post($url, $data);
        if (!$response){
            throw new SmsOnlineException($this->getError());
        }
        $answer->setResponse($response);
        $answer->checkErrors();
        return $answer;
    }

}